
package com.lmz.task.quartz.controller;

import com.lmz.task.quartz.entity.PageUtils;
import com.lmz.task.quartz.entity.QuartzEntity;
import com.lmz.task.quartz.entity.R;
import com.lmz.task.quartz.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * description:定时任务
 *
 * @author lmz
 * @company Peter
 * @date 2020/7/27  12:01
 * @expection
 * @return
 */
@RestController
@RequestMapping("/sys/scheduleLog")
public class ScheduleJobController {
    private final TaskService taskService;

    @Autowired
    public ScheduleJobController(TaskService taskService) {
        this.taskService = taskService;
    }

    /**
     * 定时任务列表
     */
    @PostMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = taskService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 定时任务信息
     */
    @GetMapping("/info/{logId}")
    public R info(@PathVariable("logId") Long logId) {
        QuartzEntity log = taskService.getById(logId);
        return R.ok().put("log", log);
    }

    /**
     * 保存定时任务
     */
    @PostMapping("/save")
    public R save(@RequestBody QuartzEntity scheduleJob) {
        taskService.saveJob(scheduleJob);
        return R.ok();
    }

    /**
     * 修改定时任务
     */
    @PostMapping("/update")
    public R update(@RequestBody QuartzEntity scheduleJob) {
        taskService.update(scheduleJob);
        return R.ok();
    }

    /**
     * 删除定时任务
     */
    @PostMapping("/delete")
    public R delete(@RequestBody Long[] jobIds) {
        taskService.deleteBatch(jobIds);
        return R.ok();
    }

    /**
     * 立即执行任务
     */
    @PostMapping("/run")
    public R run(@RequestBody Long[] jobIds) {
        taskService.run(jobIds);
        return R.ok();
    }

    /**
     * 暂停定时任务
     */
    @PostMapping("/pause")
    public R pause(@RequestBody Long[] jobIds) {
        taskService.pause(jobIds);
        return R.ok();
    }

    /**
     * 恢复定时任务
     */
    @PostMapping("/resume")
    public R resume(@RequestBody Long[] jobIds) {
        taskService.resume(jobIds);
        return R.ok();
    }
}

package com.lmz.task.quartz;

import org.springframework.stereotype.Component;

/**
 * @author lmz
 * @projectName springboot-quartz
 * @packageName com.lmz.task.quartz
 * @company Peter
 * @date 2020/7/27  11:45
 * @description
 */
@Component("testTask")
public class TaskTest {
public  void run(String params){
    System.out.println("-----------WatchCheck-----------定时任务正在执行" +params+  "," + "当前类=TaskTest.run()");
}
}

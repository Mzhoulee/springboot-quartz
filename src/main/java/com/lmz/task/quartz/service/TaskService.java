package com.lmz.task.quartz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lmz.task.quartz.entity.PageUtils;
import com.lmz.task.quartz.entity.QuartzEntity;

import java.util.List;
import java.util.Map;

/**
 * @author lmz
 * @projectName springboot-quartz
 * @packageName com.lmz.task.quartz.service
 * @company Peter
 * @date 2020/7/27  10:39
 * @description
 */
public interface TaskService extends IService<QuartzEntity> {
    /**
     * description: 列表查询
     *
     * @param params params
     * @return com.lmz.task.quartz.entity.PageUtils
     * @author lmz
     * @company Peter
     * @date 2020/8/31  10:56
     * @expection
     */
    PageUtils queryPage(Map<String, Object> params);

    /**
     * description:保存定时任务
     *
     * @param scheduleJob scheduleJob
     * @return void
     * @author lmz
     * @company Peter
     * @date 2020/7/27  10:57
     * @expection
     */
    void saveJob(QuartzEntity scheduleJob);

    /**
     * description: 更新定时任务
     *
     * @param scheduleJob scheduleJob
     * @return void
     * @author lmz
     * @company Peter
     * @date 2020/7/27  10:58
     * @expection
     */
    void update(QuartzEntity scheduleJob);

    /**
     * description:批量删除定时任务
     *
     * @param jobIds jobIds
     * @return void
     * @author lmz
     * @company Peter
     * @date 2020/7/27  10:58
     * @expection
     */
    void deleteBatch(Long[] jobIds);

    /**
     * description:批量更新定时任务状态
     *
     * @param list list
     * @return int
     * @author lmz
     * @company Peter
     * @date 2020/7/27  10:58
     * @expection
     */
    boolean updateBatch(List<QuartzEntity> list);

    /**
     * description:立即执行
     *
     * @param jobIds jobIds
     * @return void
     * @author lmz
     * @company Peter
     * @date 2020/7/27  10:58
     * @expection
     */
    void run(Long[] jobIds);

    /**
     * description:暂停运行
     *
     * @param jobIds jobIds
     * @return void
     * @author lmz
     * @company Peter
     * @date 2020/7/27  10:58
     * @expection
     */
    void pause(Long[] jobIds);

    /**
     * description:恢复运行
     *
     * @param jobIds jobIds
     * @return void
     * @author lmz
     * @company Peter
     * @date 2020/7/27  10:58
     * @expection
     */
    void resume(Long[] jobIds);
}

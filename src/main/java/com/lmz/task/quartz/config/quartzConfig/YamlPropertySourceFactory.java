package com.lmz.task.quartz.config.quartzConfig;

import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.core.io.support.PropertySourceFactory;

import java.io.IOException;
import java.util.Properties;

/**
 * description: 支持yml文件配置的读取配置类
 *
 * @author lmz
 * @company Peter
 * @date 2020/7/30  10:16
 * @expection
 * @return
 */
public class YamlPropertySourceFactory implements PropertySourceFactory {
    @Override
    public PropertySource<?> createPropertySource(String name, EncodedResource encodedResource) throws IOException {
        YamlPropertiesFactoryBean factory = new YamlPropertiesFactoryBean();
        factory.setResources(encodedResource.getResource());
        factory.afterPropertiesSet();
        Properties ymlProperties = factory.getObject();
        String propertyName = name != null ? name : encodedResource.getResource().getFilename();
        assert propertyName != null;
        assert ymlProperties != null;
        return new PropertiesPropertySource(propertyName, ymlProperties);
    }
}
package com.lmz.task.quartz.config;


import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @Author: : lmz
 * @Date: : 2018/8/26 09:33
 * 公共字段填充处理器
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        Object fieldValue = getFieldValByName("updateTime",metaObject);

        //如果该字段没有设置值
        if(fieldValue == null){
            // 起始版本 3.3.0(推荐使用)
            this.strictInsertFill(metaObject, "updateTime", LocalDateTime.class, LocalDateTime.now());

        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        Object fieldValue = getFieldValByName("updateTime",metaObject);
        Object fieldValue2 = getFieldValByName("flowState",metaObject);
        if (fieldValue2==null){
            this.strictInsertFill(metaObject,"flowState",Integer.class,1);
        }
        //如果该字段没有设置值
        if(fieldValue == null){
            this.strictInsertFill(metaObject, "updateTime", LocalDateTime.class, LocalDateTime.now()); // 起始版本 3.3.0(推荐使用)
            //this.fillStrategy(metaObject, "updateTime", LocalDateTime.now()); // 也可以使用(3.3.0 该方法有bug请升级到之后的版本如`3.3.1.8-SNAPSHOT`)
            /* 上面选其一使用,下面的已过时(注意 strictInsertFill 有多个方法,详细查看源码) */
            //this.setFieldValByName("operator", "Jerry", metaObject);
            //this.setInsertFieldValByName("operator", "Jerry", metaObject);
            //获取需要填充的字段
            //那就将其设置为"林志玲"
            //setFieldValByName("updateTime","林志玲",metaObject);
        }
    }
}

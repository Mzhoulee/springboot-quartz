package com.lmz.task.quartz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lmz.task.quartz.entity.QuartzEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author lmz
 * @projectName springboot-quartz
 * @packageName com.lmz.task.quartz
 * @company Peter
 * @date 2020/7/27  10:53
 * @description
 */
@Mapper
public interface TaskMapper extends BaseMapper<QuartzEntity> {
}
